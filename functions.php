<?php
declare(strict_types = 1);
require_once "config.php";

//Function for writing array to json-file
function arrayToJson (string $fileJson, array $array)
{
    $directory = ROOT_PATH . DIRECTORY_SEPARATOR;
    file_put_contents($directory . $fileJson , json_encode($array));
}

//Function that creates data base from array
function createDataBase (array $array, array $keys): array
{
    $i = 0;
    $j = 0;
    $k = 0;
    $sizeKeys = count($keys);
    foreach ($array as $val) {
        $dataBase[$k][$keys[$j++]] = $array[$i++];
        if ($j == $sizeKeys) {
            $j = 0;
            $k++;
        }
    }
    return $dataBase;
}

//Function that finds id
function findId (array $array, string $key, string $value): int
{
    $id = 0;
    foreach ($array as $val) {
        if ($val[$key] == $value) {//if $value was finded
            $id = (int) $val['id'];
            break;
        }
    }
    return $id;
}

function findById (array $array, int $id, string $key): string
{
    $value = "";
    foreach ($array as $val) {
        if ($val['id'] == $id) {
            $value = $val[$key];
            break;
        }
    }
    return $value;
}

function rewriteById (array $array, int $id, string $key, string $value): array
{
    foreach ($array as $k => $val) {
        if ($val['id'] == $id) {
            break;
        }
    }
    $array[$k][$key] = $value;
    return $array;
}

//Function that delete subarray by id
function deleteById (array $array, int $id): array
{
    foreach ($array as $k => $val) {
        if ($val['id'] == $id) {
            break;
        }
    }
    unset($array[$k]);
    return $array;
}

//Function that create subarray with own id
function createSubarray (array $array, array $subArray): array
{
    //$arrayKeys = array_keys($array[0]);
    $arrayKeys = ["id", "name", "login", "password", "email", "lang", "accessNum"];
    //defining number of last key
    $lastKey = array_key_last($array);
    //defining new id for new subarray
    $newId = $array[$lastKey]['id'] + 1;
    $subArray[0] = $newId;
    //Creating $newArray from $subArray and $arrayKeys
    $i = 0;
    foreach ($subArray as $val) {
        $newArray[$arrayKeys[$i++]] = (string) $val;
    }
    //Pushing $newArray to $array
    $array[] = $newArray;
    return $array;
}

//Function for opening and creating (if file isn't exists) json-file with user's data
function openCreateUsersJson(string $fileJson, string $blankTxt): array
{
    $directory = ROOT_PATH . DIRECTORY_SEPARATOR;
    if (! file_exists($directory . $fileJson)) {
        //Reading text file
        $fileString = file_get_contents($directory . $blankTxt);
        //Replacing symbols \t\n\r\0\x0B to space 
        $replSymbols = ["\t", "\r", "\n", "\0", "\x0B"];
        $fileString = str_replace($replSymbols, " ", $fileString); 
        //Deleting repeated spaces
        $fileString = preg_replace('/^ +| +$|( ) +/m', '$1', $fileString);
        //echo $fileString . "<br>";
        //Exploding string into array
        $fileArray = explode(" ", $fileString);
        //Creating users array
        $arrayKeys = ["id", "name", "login", "password", "email", "lang", "accessNum"];

        $array = createDataBase ($fileArray, $arrayKeys);

        arrayToJson ($fileJson, $array);
    }
        
    $usersJson = file_get_contents($directory . $fileJson);
    $array = json_decode($usersJson, true);
    return $array;
}

//Function for check login and password
function checkLoginPsw(array $array, string $login, string $psw, string $fileJson): bool
{
    $id = findId ($array, 'login', $login);
    if (isset($id)) { //If id by login was finded
        //Checking the password
        $passById = findById ($array, $id, 'password');
        if ($passById == $psw) {
            $_SESSION['id'] = $id;
            return true;
        }
    }
    unset($_SESSION['id']);
    return false;
}

//Function that displaying wrong entering login or password
function wrongLoginPassword ()
{
    $wrongLoginPassword = true;
    require_once ROOT_PATH . DIRECTORY_SEPARATOR . "entrance.php";
}

function emptyLoginPassword ()
{
    $emptyLoginPassword = true;
    require_once ROOT_PATH . DIRECTORY_SEPARATOR . "entrance.php";
}

//Function that displaying wrong entering email
function wrongEmail ()
{
    $wrongEmail = true;
    require_once ROOT_PATH . DIRECTORY_SEPARATOR . "forgot.php";
}

//Function that displaying empty field for email
function emptyEmail()
{
    $emptyEmail = true;
    require_once ROOT_PATH . DIRECTORY_SEPARATOR . "forgot.php";
}

//Function that displaying what all needed fields wasn't written
function wrongField()
{
    $wrongField = true;
    require_once ROOT_PATH . DIRECTORY_SEPARATOR . "register.php";
}

//Function that displaying what some fields for user is repeated
function wrongRepeat(string $field)
{
    $wrongRepeat = $field;
    require_once ROOT_PATH . DIRECTORY_SEPARATOR . "register.php";
}

//Function that displaying unaccetable password length
function wrongLengthPassword()
{
    $wrongLengthPassword = true;
    require_once ROOT_PATH . DIRECTORY_SEPARATOR . "register.php";
}

//Function for increment counter of site access for $i-user
function accessStatistics(array $users, string $fileJson, int $userId): array
{
    $directory = ROOT_PATH . DIRECTORY_SEPARATOR;
    $accessNum = findById ($users, $userId, 'accessNum');
    $accessNum++;
    $accessNum = (string) $accessNum;
    $users = rewriteById ($users, $userId, 'accessNum', $accessNum);
    file_put_contents($directory . $fileJson , json_encode($users));
    return $users;
}

//Function for check email
function checkEmail(array $array, string $email, string $fileJson): bool
{
    $id = findId ($array, 'email', $email);
    if ($id) { //If id by email was finded
        $_SESSION['id'] = $id;
        return true;
    }
    unset($_SESSION['id']);
    return false;
}

//Function that displays greeting
function greeting($lang){
    switch($lang){
        case 'en':
            $hello = "Hello, ";
        break;
        case 'ua':
            $hello = "Привіт, ";
        break;
        case 'ru':
            $hello = "Привет, ";
        break;
        case 'de':
            $hello = "Hallo, ";
        break;
        case 'fr':
            $hello = "Bonjour, ";
        break;
        case 'sp':
            $hello = "Hola, ";
        break;
        case 'it':
            $hello = "Ciao, ";
        break;
        default:
            $hello = "Hello, ";
    }
    return $hello;
}

//Function that checks matches in array
function match(array $array, string $key, string $value)
{
    $id = findId($array, $key, $value);
    if ($id) {
        return $id;
    } else {
        return false;
    }

    /*$match = false;
    $i = 1;
    foreach ($array[$key] as $val) {
        if ($value == $val) {
            $match = $i;
            break;
        }
        $i++;
    }
    return $match;*/
}

//Function for deleting all cookies
function deleteAllCookies()
{
    $past = time() - 3600;
    foreach ( $_COOKIE as $key => $value )
    {
        setcookie( $key, $value, $past, '/' );
    }
}