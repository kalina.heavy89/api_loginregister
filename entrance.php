<?php
require_once "config.php";
?>

<!DOCTYPE>
<html>
<head>
    <meta charset="utf-8">
    <title>Site entrance</title>
</head>
<body>
    <form action = "login_check.php" method = "post">
        <p>
            <label>Login</label>
            <input type = "text" name = "login" value = 
            <?php if (isset($_COOKIE['login'])) { 
                echo $_COOKIE['login'];
            } elseif (isset($_SESSION['login'])) {
                echo $_SESSION['login'];
            } else { ?>
                ""
            <?php } ?> >
        </p>
        <p>
            <label>Password</label>
            <input type = "password" name = "password" value = 
             <?php if (isset($_COOKIE['password'])) { 
                echo $_COOKIE['password'];
            } elseif (isset($_SESSION['login'])) {
                echo $_SESSION['login'];
            } else { ?>
                ""
            <?php } ?> >
        </p>
        <div><input type="checkbox" name="remember" id="remember" 
                <?php if(isset($_COOKIE["remember"])) { ?> checked <?php } ?> />
            <label for="remember-me">Remember me</label>
        </div>
            <input type = "submit" name="action" value = "Login" />
            <span class="psw">Forgot <a href="/forgot.php">password?</a></span>
    </form>

    <form action = "register.php">
        <div>
            <div> User registration </div>
            <input type = "submit" name = "action" value = "Registration" />
        </div>
    </form>

    <?php if(isset($wrongLoginPassword)): ?>
        <h3>Login or password is wrong</h3>
    <?php endif;
    if (isset($emptyLoginPassword)): ?>
        <h3>Login or password is empty</h3>
    <?php endif; ?>

</body>
</html>