<?php
require_once "config.php";
$directory = ROOT_PATH . DIRECTORY_SEPARATOR;
require_once "functions.php";
//define(SALT1, "&ds56sYG41g%");
//define(SALT2, "54sd5SdV");

//var_dump($_POST);
//var_dump($_COOKIE);

//Reading file that consists of users data
$users = openCreateUsersJson('users.json', 'users.txt');

if ($_POST['action'] == 'Remember') { //If remembering login and password by email
    if (!empty($_POST['email'])) {
        $_SESSION['email'] = $_POST['email'];
        $checkBool = checkEmail($users, $_SESSION['email'], "users.json");
        if ($checkBool == true) {//If user was finded
            $_SESSION['login'] = findById ($users, $_SESSION['id'], 'login');
            $_SESSION['password'] = findById ($users, $_SESSION['id'], 'password');
            //Moving to the forgot.php
            require $directory . "forgot.php";
        } else {
            wrongEmail();
        }
    } else {
        session_unset();
        emptyEmail();
    }
} elseif (($_POST['action'] == 'Login')) {
    $_SESSION['login'] = $_POST['login'];
    $_SESSION['password'] = $_POST['password'];
    if (!empty($_POST['login']) || !empty($_POST['password'])) {
        //Finding login and checking the password
        $checkBool = checkLoginPsw($users, $_POST['login'], $_POST['password'], "users.json");
        if ($checkBool == true) {//If user was finded
            //Incrementing number of accesses for finded user
            $users = accessStatistics($users, "users.json", $_SESSION['id']);

            $_SESSION['name'] = findById ($users, $_SESSION['id'], 'name');
            $_SESSION['lang'] = findById ($users, $_SESSION['id'], 'lang');
            if (isset($_POST['remember'])) { //Setting the cookies
                $hour = time() + 3600 * 24 * 30;
                setcookie('remember', $_POST['remember'], $hour);
                setcookie('login', $_POST['login'], $hour);
                setcookie('password', $_POST['password'], $hour);
            } else { //Unsetting the cookies
                $hour = time() - 3600;
                setcookie('remember', "", $hour);
                setcookie("login", "", $hour);
                setcookie("password", "", $hour);
            }

            //Moving to the site page
            require $directory . "site_page.php";
            //header('Location: /API/site_page.php');
        } else { //If login or password is wrong
            //session_unset();
            wrongLoginPassword ();
        } 
    } else { //if login or password is empty
        emptyLoginPassword ();
    }
} elseif ($_POST['action'] == 'Exit') {
    session_unset();
    require $directory . "entrance.php";
} elseif ($_POST['action'] == 'Delete') {
    $users = deleteById ($users, $_SESSION['id']);
    arrayToJson ('users.json', $users);
    session_unset();
    deleteAllCookies();
    require $directory . "entrance.php";       
} else {
    ;
}