<?php
require_once "config.php";

$languages=[
        "en" => "english",
        "ua" => "українська",
        "ru" => "русский",
        "de" => "deutsch",
        "it" => "italiano",
        "fr" => "français",
        "sp" => "español"
    ];
?>

<!DOCTYPE>
<html>
<head>
    <meta charset="utf-8">
    <title>Site entrance</title>
</head>
<body>
    <h1> Registration </h1>
    <form action = "reg_check.php" method = "post">
        <p>
            <label>Name *</label>
            <input type = "text" name = "name" value = 
                <?php if (isset($_SESSION['name'])) { 
                    echo $_SESSION['name'];
                } else { ?>
                    ""
                <?php } ?>
            >
        </p>
        <p>
            <label>Login *</label>
            <input type = "text" name = "login" value = 
                <?php if (isset($_SESSION['login'])) { 
                    echo $_SESSION['login'];
                } else { ?>
                    ""
                <?php } ?>
            >
        </p>
        <p>
            <label>Password *</label>
            <input type = "password" name = "password" value = 
                <?php if (isset($_SESSION['password'])) { 
                    echo $_SESSION['password'];
                } else { ?>
                    ""
                <?php } ?>
            >
        </p>
        <p>
            <label>Email *</label>
            <input type = "text" name = "email" value = 
                <?php if (isset($_SESSION['email'])) { 
                    echo $_SESSION['email'];
                } else { ?>
                    ""
                <?php } ?>
            >
        </p>
        <label for="languages"><b>Languages</b></label>
        <p>
            <select name="lang">
                <?php foreach($languages as $key=>$val){?>
                    <option value = <?= $key ?> 
                        <?php
                            if (isset($_SESSION['lang'])):
                                if ($key == $_SESSION['lang']): ?>
                                    selected
                                <?php endif;
                            endif;
                        ?>
                    > <?= $val ?> </option>
                <?php } ?>
            </select>
        </p>
        <div><input type="checkbox" name="remember" id="remember" 
                <?php if(isset($_COOKIE["remember"])) { ?> checked <?php } ?> />
            <label for="remember-me">Remember me</label>
        </div>
            <input type = "submit" name="action" value = "Register">
    </form>

    <form action = "entrance.php">
        <?php 
            session_unset();
        ?>
        <input type = "submit" name="action" value = "Exit">
    </form>

    <?php //Section that displays errors
        //var_dump($_SESSION);
        if (isset($wrongField)) {
            echo "All fields marked with an asterisk must be written!";
            die();
        } elseif (isset($wrongRepeat)) {
            echo "User with this " . $wrongRepeat . " is exists!";
            die();
        } elseif (isset($wrongLengthPassword)) {
            echo "Password must have from 7 to 13 symbols";
            die();
        } else {
            ;
        }
    ?>
</body>
</html>