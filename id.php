<?php

declare(strict_types = 1);
require_once "config.php";
$directory = ROOT_PATH . DIRECTORY_SEPARATOR;

//Reading file that consists of users data
$users = openCreateUsersJson('users.json', 'users.txt');

/*//Function that creates data base from array
function createDataBase (array $array, array $keys): array
{
    $i = 0;
    $j = 0;
    $k = 0;
    $sizeKeys = count($keys);
    foreach ($array as $val) {
        $dataBase[$k][$keys[$j++]] = $array[$i++];
        if ($j == $sizeKeys) {
            $j = 0;
            $k++;
        }
    }
    return $dataBase;
}*/

//Function that finds id
function findId (array $array, string $key, string $value): int
{
    foreach ($array as $val) {
        if ($val[$key] == $value) {//if $value was finded
            $id = (int) $val['id'];
            break;
        }
    }
    return $id;
}

function findById (array $array, int $id, string $key): string
{
    foreach ($array as $val) {
        if ($val['id'] == $id) {
            $value = $val[$key];
            break;
        }
    }
    return $value;
}

function rewriteById (array $array, int $id, string $key, string $value): array
{
    foreach ($array as $k => $val) {
        if ($val['id'] == $id) {
            break;
        }
    }
    $array[$k][$key] = $value;
    return $array;
}

//Function that delete subarray by id
function deleteById (array $array, int $id): array
{
    foreach ($array as $k => $val) {
        if ($val['id'] == $id) {
            break;
        }
    }
    unset($array[$k]);
    return $array;
}

//Function that create subarray with own id
function createSubarray (array $array, array $subArray): array
{
    $arrayKeys = array_keys($array[0]);
    //defining number of last key
    $lastKey = array_key_last($array);
    //defining new id for new subarray
    $newId = $array[$lastKey]['id'] + 1;
    $subArray[0] = $newId;
    //Creating $newArray from $subArray and $arrayKeys
    $i = 0;
    foreach ($subArray as $val) {
        $newArray[$arrayKeys[$i++]] = (string) $val;
    }
    //Pushing $newArray to $array
    $array[] = $newArray;
    return $array;
}

//Finding id by 'login':
$login = 'NaomiCamplell';
$id = findId($users, 'login', $login);
echo "Id: " . $id . "<br>";

//Finding password by id:
$password = findById ($users, $id, 'password');
echo "Password: " . $password . "<br>";

//Rewriting by id
$accessNum = findById ($users, $id, 'accessNum');
//$accessNum = (int) $accessNum + 1;
$accessNum++;
$accessNum = (string) $accessNum;
var_dump($accessNum);
echo "<br>";
$users = rewriteById ($users, $id, 'accessNum', $accessNum);

//Deleting by id
$users = deleteById ($users, 6);

//Creating subarray with own unique id
$userData = [0, 'newUser', 'newUser', '1111111', 'newUser@mail.com', 'ua', 0];
$users = createSubarray ($users, $userData);
echo "<pre>";
var_dump($users);
echo "</pre>";