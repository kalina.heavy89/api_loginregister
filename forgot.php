<?php
require_once "config.php";

$hour = time() - 3600;
setcookie('remember', "", $hour);
setcookie("login", "", $hour);
setcookie("password", "", $hour);

?>

<!DOCTYPE>
<html>
<head>
    <meta charset="utf-8">
    <title>Site entrance</title>
</head>
<body>
    <form action = "login_check.php" method = "post">
        <p>
            <label>Email</label>
            <input type = "text" name = "email" value = 
            <?php if (isset($_SESSION['email'])) { 
                echo $_SESSION['email'];
            } else { ?>
                ""
            <?php } ?> />
        </p>
        <div>
            <input type = "submit" name = "action" value = "Remember" />
        </div>
        <p>
            <label>Login</label>
            <input type = "text" name = "login" value = 
            <?php if (isset($_SESSION['login'])) { 
                echo $_SESSION['login'];
            } else { ?>
                ""
            <?php } ?> >
        </p>
        <p>
            <label>Password</label>
            <input type = "text" name = "password" value = 
             <?php if (isset($_SESSION['password'])) { 
                echo $_SESSION['password'];
            } else { ?>
                ""
            <?php } ?> >
        </p>
        <div><input type="checkbox" name="remember" id="remember" 
                <?php if(isset($_SESSION["remember"])) { ?> checked <?php } ?> />
            <label for="remember-me">Remember me</label>
        </div>
        <input type = "submit" name = "action" value = "Login" />
    </form>

    <?php if(isset($wrongEmail)): ?>
        <h3>User with this email isn't exists</h3>
    <?php endif;
    if (isset($emptyEmail)): ?>
        <h3> Field for email is empty </h3>
    <?php endif; ?>
</body>
</html>