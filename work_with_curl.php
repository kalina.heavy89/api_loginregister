<?php

//Getting information about all registered users
// create curl resource
$ch = curl_init();
//set url
curl_setopt($ch, CURLOPT_URL, "http://API/users.json");
//return the transfer as a string
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//$output contains the output string
$output = curl_exec($ch);
//close curl resource to free up system resources
curl_close($ch);

$users = json_decode($output, true);

echo "<pre>";
var_dump($users);
echo "</pre>";

//Getting information about last user
$id = $users[count($users)]['id'];
echo "Last id: " . $id . "<br>";
// create curl resource
$ch = curl_init();
//set url
curl_setopt($ch, CURLOPT_URL, "http://API/users.json/9/10");
//return the transfer as a string
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//$output contains the output string
$output = curl_exec($ch);
//close curl resource to free up system resources
curl_close($ch);

echo "<pre>";
print_r(json_decode($output, true));
echo "</pre>";