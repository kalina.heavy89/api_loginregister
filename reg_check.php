<?php
require_once "config.php";
$directory = ROOT_PATH . DIRECTORY_SEPARATOR;
require_once "functions.php";

//Reading file that consists of users data
$users = openCreateUsersJson('users.json', 'users.txt');
//$checkBool = [];

$_SESSION['name'] = $_POST['name'];
$_SESSION['login'] = $_POST['login'];
$_SESSION['password'] = $_POST['password'];
$_SESSION['email'] = $_POST['email'];

//Checking that all needed fields was written
if ((empty($_POST['name'])) || (empty($_POST['login'])) || (empty($_POST['password'])) || (empty($_POST['email']))) {
    wrongField();
}

$_SESSION['lang'] = $_POST['lang'];

//Checking that fields ['login'], ['password'] and ['email'] nowhere repeated
if (match($users, 'login', $_SESSION['login'])) {
    wrongRepeat('login');
} elseif (match($users, 'password', $_SESSION['password'])) {
    wrongRepeat('password');
} elseif (match($users, 'email', $_SESSION['email'])) {
    wrongRepeat('email');
} else {;}

//Checking length of password (it must be from 7 to 13 symbols)
if ((strlen($_SESSION['password']) < 7) || (strlen($_SESSION['password']) > 13)) {
    wrongLengthPassword();
}

//Writing data from new user to array $users
$userData = [0, $_SESSION['name'], $_SESSION['login'], $_SESSION['password'], $_SESSION['email'], $_SESSION['lang'], 0];
/*$usersKey = array_keys($users);
$i = 0;
foreach ($usersKey as $key) {
    $users[$key][] = $userData[$i];
    $i++;
}*/
$users = createSubarray ($users, $userData);

arrayToJson ('users.json', $users);

//Entering into the site
$_SESSION['userName'] = $_SESSION['name'];
if (isset($_POST['remember'])) { //Setting the cookies
    $hour = time() + 3600 * 24 * 30;
    setcookie('remember', $_POST['remember'], $hour);
    setcookie('login', $_POST['login'], $hour);
    setcookie('password', $_POST['password'], $hour);
} else { //Unsetting the cookies
    $hour = time() - 3600;
    setcookie('remember', "", $hour);
    setcookie("login", "", $hour);
    setcookie("password", "", $hour);
}
require $directory . "site_page.php";